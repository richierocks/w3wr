

w3wError <- function(status, call = NULL)
{
  classes <- c("w3wError", "w3wCondition", "simpleError", "error", "condition")
  status_string <- paste(names(status), status, sep = "=", collapse = "\n")
  msg <- paste(
    "There was a problem retrieving the content from what3words.com.",
    "See https://docs.what3words.com/api/v2/#errors for more details.",
    status_string,
    sep = "\n"
  )

  structure(
    list(
      message = msg,
      call = call,
      status = status
    ),
    class = classes
  )
}


