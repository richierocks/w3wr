BASE_URL <- "https://api.what3words.com/v2"
FORWARD_URL <- file.path(BASE_URL, "forward", fsep = "/")
REVERSE_URL <- file.path(BASE_URL, "reverse", fsep = "/")

#' Access w3w API and parse the result
#'
#' Utility function to access the what3words.com API and parse the result.
#' @param full_url A string giving the URL to retrieve data from.
#' @references \url{https://docs.what3words.com/api/v2}
#' @importFrom httr GET
#' @importFrom httr content
#' @importFrom httr stop_for_status
#' @noRd
get_location <- function(full_url)
{
  response <- GET(full_url)
  stop_for_status(response)
  value <- content(response)

  if(value$status$reason != "OK" || !is.null(value$status$code))
  {
    err <- w3wError(value$status)
    stop(err)
  }

  locations <- with(
    value,
    data.frame(
      words = words,
      lng   = geometry$lng,
      lat   = geometry$lat,
      lng.west = bounds$southwest$lng,
      lng.east = bounds$northeast$lng,
      lat.south = bounds$southwest$lat,
      lat.north = bounds$northeast$lat,
      map_url   = map,
      stringsAsFactors = FALSE
    )
  )
  class(locations) <- c("w3w_location", "data.frame")
  locations
}

#' Get location from what3words identifiers
#'
#' @param addr A character vector of length 3 containing what3words.com
#' identifiers.
#' @param api_key A string giving your what3words.com API key.
#' @examples
#' \donttest{
#' # Not tested as API key needs to be set
#' get_location_from_words("happier.clocked.gliders")
#' }
#' @importFrom assertive.types assert_is_character
#' @export
get_location_from_words <- function(addr, api_key = get_api_key())
{
  assert_is_character(addr)
  addr <- tolower(addr)
  bad <- !grepl("^[a-z]+\\.[a-z]+\\.[a-z]+$", addr)
  if(any(bad))
  {
    stop(
      "The following addresses are not in the form name1.name2.name3:\n",
      toString(addr[bad])
    )
  }
  locations <- lapply(
    addr,
    get_location_from_words_single,
    api_key = api_key
  )
  do.call(rbind, locations)
}

#' @importFrom httr modify_url
get_location_from_words_single <- function(addr, api_key)
{
  # need to keep display = "full" for lower/upper bounds of coords
  full_url <- modify_url(FORWARD_URL, query = list(addr = addr, key = api_key))
  get_location(full_url)
}

#' Get location from latitude and longitude
#'
#' @param lat A numeric vector with values from -90 to 90 representing latitude.
#' @param lng A numeric vector with values from -180 to 180 representing longitude.
#' @param api_key A string giving your what3words.com API key.
#' @examples
#' \donttest{
#' # Not tested as API key needs to be set
#' get_location_from_coords(lat = 25.31771, lng = 51.43863)
#' }
#' @importFrom assertive.types assert_is_numeric
#' @importFrom assertive.numbers assert_all_are_in_closed_range
#' @export
get_location_from_coords <- function(lat, lng, api_key = get_api_key())
{
  assert_is_numeric(lat)
  assert_is_numeric(lng)
  assert_all_are_in_closed_range(lat, lower = -90, upper = 90)
  assert_all_are_in_closed_range(lng, lower = -180, upper = 180)

  coords <- paste(lat, lng, sep = ",")
  locations <- lapply(
    coords,
    get_location_from_coords_single,
    api_key = api_key
  )
  do.call(rbind, locations)
}


#' @importFrom httr modify_url
get_location_from_coords_single <- function(coords, api_key)
{
  full_url <- modify_url(REVERSE_URL, query = list(coords = coords, key = api_key))
  get_location(full_url)
}
