[![Project Status: Active - The project has reached a stable, usable state and is being actively developed.](http://www.repostatus.org/badges/0.1.0/active.svg)](http://www.repostatus.org/#active)

[![Build Status](https://semaphoreci.com/api/v1/richierocks/w3wr/branches/master/badge.svg)](https://semaphoreci.com/richierocks/w3wr)
[![Build status](https://ci.appveyor.com/api/projects/status/dqj7ri04mp8dj0se?svg=true)](https://ci.appveyor.com/project/richierocks/w3wr)


# w3wr: An R package to access the what3words.com API

[what3words.com](https://what3words.com/about) is a mapping application that splits the world into a grid of 3m by 3m cells, each identified by a three word key in the form "word1.word2.word3".  The idea is that you can specify your location more accurately than a post code or zip code, helping emergency services and people delivering things.

This package provides an R interface to the [what3words API](https://docs.what3words.com/api/v2).


## Installation

To install the stable version (coming soon), type:


```r
install.packages("w3wr")
```

To install the development version, you first need the *devtools* package.


```r
install.packages("devtools")
```

Then you can install the *w3wr* package using


```r
devtools::install_bitbucket("richierocks/w3wr")
```

## Setup

Before you begin, you need to register with what3words.com to get an API key.  This is free, and takes around five minutes.  You need to provide a real email address in order to have your account verified.  First, visit

https://map.what3words.com/register?dev=true

(Make sure you fill out the API details in the second half of the form.)

Once you've registered your account, and received your API key, you need to make this accessible by R.  You have three options.

1. In your operating system, set an environment variable named `W3W_API_KEY`.  This will permanently store the key.
2. Use the `set_api_key` function to store the key for the rest of your R session.
3. Pass the key in each time you call one of the `get_location_*` functions.

## Getting coordinates

`get_location_from_words` accepts a character vector of what3words IDs, and returns a data frame of locations.


`get_location_from_coords` accepts a numeric vector latitudes, and another of longitudes, and returns a data frame of locations.


```r
library(w3wr)
locations_from_words <- get_location_from_words(
  c("fence.gross.bats", "survey.ulterior.boating")
)
```


|words                   |       lng|      lat|  lng.west|  lng.east| lat.south| lat.north|map_url                               |
|:-----------------------|---------:|--------:|---------:|---------:|---------:|---------:|:-------------------------------------|
|fence.gross.bats        | -0.141888| 51.50136| -0.141909| -0.141866|  51.50135|  51.50138|http://w3w.co/fence.gross.bats        |
|survey.ulterior.boating | 78.042167| 27.17493| 78.042152| 78.042182|  27.17491|  27.17494|http://w3w.co/survey.ulterior.boating |


```r
library(w3wr)
locations_from_coords <- get_location_from_coords(
  lat = c(27.98786, 25.19687), 
  lng = c(86.92502, 55.27435)
)
```


|words                      |      lng|      lat| lng.west| lng.east| lat.south| lat.north|map_url                                  |
|:--------------------------|--------:|--------:|--------:|--------:|---------:|---------:|:----------------------------------------|
|lunches.founder.nimbleness | 86.92502| 27.98786| 86.92500| 86.92503|  27.98785|  27.98787|http://w3w.co/lunches.founder.nimbleness |
|universally.remain.variety | 55.27435| 25.19687| 55.27433| 55.27436|  25.19685|  25.19688|http://w3w.co/universally.remain.variety |

You can view the results in a browser using `view_locations`.


```r
view_locations(locations_from_words)
view_locations(locations_from_coords)
```

## Alternatives

Barry Rowlingson's [`what3wordsr`](https://github.com/barryrowlingson/what3wordr) package performs the same task as this package, but isn't vectorized, and hasn't been updated to use the current version of the API.

Oliver Keyes' [`threewords`](https://www.github.com/ironholds/threewords) package also does the same task.  This one is also not vectorized, and based on the old API.

